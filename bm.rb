#!/usr/bin/ruby

require 'erb'

class Bm
  attr_accessor :name
  
  def initialize
    @tmpldir = "tmpl"
    @photodir = "photos"
    @tex = ""
  end

  def add(tmpl, text: "", photos: [])
    tpath = "#{@tmpldir}/#{tmpl}.tex"

    photos = photos.map {
      |path| "#{@photodir}/#{path}"}

    t = ERB.new(File.read(tpath))
    @tex += t.result_with_hash(
      text: text, photos: photos)
  end

  def print
    texf = "#{@name}.tex"
    File.open(texf, "w") {
      |f| f.write(@tex) }
    system("xelatex", texf)
  end
end
