#!/usr/bin/ruby
require_relative "bm"

b = Bm.new
b.name = "mybook"

b.add "intro"

b.add "cover",
  text: "This is the Cover Page",
  photos: ["cover.jpg"]

b.add "single",
  text: "On the Beach",
  photos: ["cover.jpg"]

b.add "full-width",
  photos: ["cover.jpg"]

b.add "twotowers",
  text: "Double Vertical Pics",
  photos: ["atom.jpg", "atom.jpg"]

b.add "chapter",
  text: "Test Chapter"

b.add "outro"

b.print
